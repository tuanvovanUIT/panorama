# Panorama
**1. Cài đặt môi trường.**
```
pip install opencv-python==3.4.2.16
pip install opencv-contrib-python==3.4.2.16
conda install -c conda-forge imutils
conda install -c anaconda django
```

**2. Vào thư mục panogama, chạy cmd tại thư mục này với lệnh:**
`python manage.py runserver `
**Sau đó vào trình duyệt, truy cập vào địa chỉ:**
[Demo](http://127.0.0.1:8000/)

